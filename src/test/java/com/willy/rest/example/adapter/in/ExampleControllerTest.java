package com.willy.rest.example.adapter.in;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.willy.rest.example.Factory;
import com.willy.rest.example.adapter.dto.request.ExampleRequest;
import com.willy.rest.example.adapter.mapper.RequestMapperImpl;
import com.willy.rest.example.adapter.mapper.ResponseMapperImpl;
import com.willy.rest.example.application.dto.input.ExampleRequestDTO;
import com.willy.rest.example.application.dto.output.ExampleResponseDTO;
import com.willy.rest.example.application.port.in.ExampleInputPort;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
public class ExampleControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    ExampleInputPort inputPort;

    @SpyBean
    RequestMapperImpl requestMapper;

    @SpyBean
    ResponseMapperImpl responseMapper;

    static final String EXAMPLE_POST = "/example";
    static final String EXAMPLE_GET = "/example/{id}";

    @Test
    void shouldCreateExample() throws Exception {
        ExampleRequest request = Factory.createExampleRequest();

        when(inputPort.create(any(ExampleRequestDTO.class))).thenReturn(1);

        mockMvc.perform(
                        post(EXAMPLE_POST)
                                .accept(MediaType.APPLICATION_JSON)

                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(request)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$", is(1)));
    }

    @Test
    void shouldGetExample() throws Exception {

        ExampleResponseDTO response = Factory.createExampleResponseDTO();

        when(inputPort.getExample(anyInt())).thenReturn(Optional.of(response));

        mockMvc.perform(
                        get(EXAMPLE_GET, 1)
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(response.getId())))
                .andExpect(jsonPath("$.name", is(response.getName())))
                .andExpect(jsonPath("$.age", is(response.getAge())))
                .andExpect(jsonPath("$.bornAt", is(response.getBornAt().toString())))
        ;
    }
}
