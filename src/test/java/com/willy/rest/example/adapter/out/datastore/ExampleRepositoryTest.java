package com.willy.rest.example.adapter.out.datastore;

import com.willy.rest.example.Factory;
import com.willy.rest.example.adapter.out.datastore.entity.ExampleEntity;
import com.willy.rest.example.adapter.out.datastore.mapper.DataStoreMapperImpl;
import com.willy.rest.example.adapter.out.datastore.repository.ExampleRepository;
import com.willy.rest.example.application.dto.input.ExampleRequestDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.SpyBean;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class ExampleRepositoryTest {

    @Autowired
    ExampleRepository exampleRepository;

    @SpyBean
    DataStoreMapperImpl mapper;

    @Test
    void shouldSaveAnExample() {
        ExampleRequestDTO requestDTO = Factory.createExampleRequestDTO();

        ExampleEntity entity = exampleRepository.save(mapper.toEntity(requestDTO));

        assertEquals(requestDTO.getAge(), entity.getAge());
        assertEquals(requestDTO.getBornAt(), entity.getBornAt());
        assertEquals(requestDTO.getName(), entity.getName());
        assertTrue(entity.getId() > 0);
        assertNotNull(entity.getCreateAt());
    }

    @Test
    void shouldGetAnExample() {

        ExampleRequestDTO requestDTO = Factory.createExampleRequestDTO();

        int id = exampleRepository.save(mapper.toEntity(requestDTO)).getId();

        Optional<ExampleEntity> entity = exampleRepository.findById(id);

        assertTrue(entity.isPresent());
        assertEquals(requestDTO.getAge(), entity.get().getAge());
        assertEquals(requestDTO.getBornAt(), entity.get().getBornAt());
        assertEquals(requestDTO.getName(), entity.get().getName());
        assertTrue(entity.get().getId() > 0);
        assertNotNull(entity.get().getCreateAt());
    }
}
