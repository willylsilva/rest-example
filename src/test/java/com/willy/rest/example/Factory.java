package com.willy.rest.example;

import com.willy.rest.example.adapter.dto.request.ExampleRequest;
import com.willy.rest.example.application.dto.input.ExampleRequestDTO;
import com.willy.rest.example.application.dto.output.ExampleResponseDTO;

import java.time.LocalDate;
import java.time.Month;

public class Factory {

    public static ExampleRequest createExampleRequest() {
        return new ExampleRequest(
                "Willy",
                27,
                LocalDate.of(1994, Month.JULY, 2)
        );
    }

    public static ExampleRequestDTO createExampleRequestDTO() {
        return new ExampleRequestDTO(
                "Willy",
                27,
                LocalDate.of(1994, Month.JULY, 2)
        );
    }

    public static ExampleResponseDTO createExampleResponseDTO() {
        return new ExampleResponseDTO(
                1,
                "Willy",
                27,
                LocalDate.of(1994, Month.JULY, 2)
        );
    }
}
