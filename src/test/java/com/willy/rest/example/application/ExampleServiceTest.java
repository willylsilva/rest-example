package com.willy.rest.example.application;

import com.willy.rest.example.Factory;
import com.willy.rest.example.application.dto.input.ExampleRequestDTO;
import com.willy.rest.example.application.dto.output.ExampleResponseDTO;
import com.willy.rest.example.application.port.out.ExampleOutputPort;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ExampleServiceTest {

    @InjectMocks
    ExampleService exampleService;

    @Mock
    ExampleOutputPort outputPort;

    @Test
    void shouldCreateExample() {
        when(outputPort.create(any(ExampleRequestDTO.class))).thenReturn(1);

        ExampleRequestDTO requestDTO = Factory.createExampleRequestDTO();

        int response = exampleService.create(requestDTO);

        assertEquals(1, response);
    }

    @Test
    void shouldGetExample() {
        ExampleResponseDTO expected = Factory.createExampleResponseDTO();

        when(outputPort.getExample(anyInt())).thenReturn(Optional.of(expected));

        Optional<ExampleResponseDTO> response = exampleService.getExample(1);

        assertTrue(response.isPresent());
        assertEquals(expected.getName(), response.get().getName());
        assertEquals(expected.getAge(), response.get().getAge());
        assertEquals(expected.getBornAt(), response.get().getBornAt());
        assertEquals(expected.getId(), response.get().getId());
    }
}
