package com.willy.rest.example.application;

import com.willy.rest.example.application.dto.input.ExampleRequestDTO;
import com.willy.rest.example.application.dto.output.ExampleResponseDTO;
import com.willy.rest.example.application.port.in.ExampleInputPort;
import com.willy.rest.example.application.port.out.ExampleOutputPort;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ExampleService implements ExampleInputPort {

    public ExampleService(ExampleOutputPort exampleOutputPort) {
        this.exampleOutputPort = exampleOutputPort;
    }

    private final ExampleOutputPort exampleOutputPort;

    @Override
    public int create(ExampleRequestDTO requestDTO) {
        return exampleOutputPort.create(requestDTO);
    }

    @Override
    public Optional<ExampleResponseDTO> getExample(int id) {
        return exampleOutputPort.getExample(id);
    }
}
