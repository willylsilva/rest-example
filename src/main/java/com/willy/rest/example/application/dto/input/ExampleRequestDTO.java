package com.willy.rest.example.application.dto.input;

import java.time.LocalDate;

public class ExampleRequestDTO {

    private final String name;
    private final int age;
    private final LocalDate bornAt;

    public ExampleRequestDTO(String name, int age, LocalDate bornAt) {
        this.name = name;
        this.age = age;
        this.bornAt = bornAt;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public LocalDate getBornAt() {
        return bornAt;
    }
}
