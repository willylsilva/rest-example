package com.willy.rest.example.application.port.out;

import com.willy.rest.example.application.dto.input.ExampleRequestDTO;
import com.willy.rest.example.application.dto.output.ExampleResponseDTO;

import java.util.Optional;

public interface ExampleOutputPort {
    int create(ExampleRequestDTO requestDTO);

    Optional<ExampleResponseDTO> getExample(int id);
}
