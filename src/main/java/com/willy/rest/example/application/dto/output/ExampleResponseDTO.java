package com.willy.rest.example.application.dto.output;

import java.time.LocalDate;

public class ExampleResponseDTO {

    private final int id;
    private final String name;
    private final int age;
    private final LocalDate bornAt;

    public ExampleResponseDTO(int id, String name, int age, LocalDate bornAt) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.bornAt = bornAt;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public LocalDate getBornAt() {
        return bornAt;
    }
}
