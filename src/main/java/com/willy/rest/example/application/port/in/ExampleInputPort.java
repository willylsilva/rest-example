package com.willy.rest.example.application.port.in;

import com.willy.rest.example.application.dto.input.ExampleRequestDTO;
import com.willy.rest.example.application.dto.output.ExampleResponseDTO;

import java.util.Optional;

public interface ExampleInputPort {
    int create(ExampleRequestDTO requestDTO);

    Optional<ExampleResponseDTO> getExample(int id);
}
