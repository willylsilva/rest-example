package com.willy.rest.example.adapter.out.datastore.repository;

import com.willy.rest.example.adapter.out.datastore.entity.ExampleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExampleRepository extends JpaRepository<ExampleEntity, Integer> {
}
