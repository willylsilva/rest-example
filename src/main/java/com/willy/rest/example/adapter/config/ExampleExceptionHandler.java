package com.willy.rest.example.adapter.config;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExampleExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    ResponseEntity<ExampleHandler> methodArgument(MethodArgumentNotValidException e) {
        ExampleHandler exampleHandler = new ExampleHandler(
                e.getBindingResult().getFieldError() != null ? e.getBindingResult().getFieldError().getField() : "",
                e.getMessage()
        );

        return ResponseEntity.badRequest().body(exampleHandler);
    }
}
