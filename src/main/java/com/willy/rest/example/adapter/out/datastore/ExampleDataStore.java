package com.willy.rest.example.adapter.out.datastore;

import com.willy.rest.example.adapter.out.datastore.mapper.DataStoreMapper;
import com.willy.rest.example.adapter.out.datastore.repository.ExampleRepository;
import com.willy.rest.example.application.dto.input.ExampleRequestDTO;
import com.willy.rest.example.application.dto.output.ExampleResponseDTO;
import com.willy.rest.example.application.port.out.ExampleOutputPort;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class ExampleDataStore implements ExampleOutputPort {

    private final ExampleRepository exampleRepository;
    private final DataStoreMapper mapper;

    public ExampleDataStore(ExampleRepository exampleRepository, DataStoreMapper mapper) {
        this.exampleRepository = exampleRepository;
        this.mapper = mapper;
    }

    @Override
    public int create(ExampleRequestDTO requestDTO) {
        return exampleRepository.save(mapper.toEntity(requestDTO)).getId();
    }

    @Override
    public Optional<ExampleResponseDTO> getExample(int id) {
        return exampleRepository.findById(id).map(mapper::toDTO);
    }
}