package com.willy.rest.example.adapter.dto.request;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDate;

public class ExampleRequest {

    @NotBlank(message = "Name must be fill")
    private String name;
    @Min(value = 0, message = "Age must be greater than 0")
    private int age;
    @NotNull(message = "Born must be fill")
    @PastOrPresent(message = "Born must be in the past")
    private LocalDate bornAt;

    public ExampleRequest() {
    }

    public ExampleRequest(String name, int age, LocalDate bornAt) {
        this.name = name;
        this.age = age;
        this.bornAt = bornAt;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public LocalDate getBornAt() {
        return bornAt;
    }
}
