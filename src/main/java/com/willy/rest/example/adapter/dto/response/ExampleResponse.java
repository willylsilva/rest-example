package com.willy.rest.example.adapter.dto.response;

import java.time.LocalDate;

public class ExampleResponse {

    private final int id;
    private final String name;
    private final int age;
    private final LocalDate bornAt;

    public ExampleResponse(int id, String name, int age, LocalDate bornAt) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.bornAt = bornAt;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public LocalDate getBornAt() {
        return bornAt;
    }
}
