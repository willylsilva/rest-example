package com.willy.rest.example.adapter.mapper;

import com.willy.rest.example.adapter.dto.response.ExampleResponse;
import com.willy.rest.example.application.dto.output.ExampleResponseDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ResponseMapper {

    ResponseMapper INSTANCE = Mappers.getMapper(ResponseMapper.class);

    ExampleResponse toResponse(ExampleResponseDTO exampleResponseDTO);
}
