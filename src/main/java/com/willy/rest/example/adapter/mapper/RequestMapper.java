package com.willy.rest.example.adapter.mapper;

import com.willy.rest.example.adapter.dto.request.ExampleRequest;
import com.willy.rest.example.application.dto.input.ExampleRequestDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface RequestMapper {

    RequestMapper INSTANCE = Mappers.getMapper(RequestMapper.class);

    ExampleRequestDTO toRequestDTO(ExampleRequest request);
}
