package com.willy.rest.example.adapter.out.datastore.mapper;

import com.willy.rest.example.adapter.out.datastore.entity.ExampleEntity;
import com.willy.rest.example.application.dto.input.ExampleRequestDTO;
import com.willy.rest.example.application.dto.output.ExampleResponseDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface DataStoreMapper {

    DataStoreMapper INSTANCE = Mappers.getMapper(DataStoreMapper.class);

    ExampleEntity toEntity(ExampleRequestDTO requestDTO);

    ExampleResponseDTO toDTO(ExampleEntity exampleEntity);
}
