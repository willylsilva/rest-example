package com.willy.rest.example.adapter.openapi;

import com.willy.rest.example.adapter.dto.request.ExampleRequest;
import com.willy.rest.example.adapter.dto.response.ExampleResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

public interface ExampleApi {

    @Operation(summary = "REST Post example")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ExampleRequest.class))}),
            @ApiResponse(responseCode = "400", description = "Bad Request",
                    content = @Content)})
    ResponseEntity<Integer> create(@Parameter(description = "Create Example")
                                   @RequestBody ExampleRequest request);

    @Operation(summary = "REST Get example")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ExampleRequest.class))}),
            @ApiResponse(responseCode = "404", description = "Not Found",
                    content = @Content)})
    ResponseEntity<ExampleResponse> getExample(@Parameter(description = "Get Example")
                                               @PathVariable int id);

}
