package com.willy.rest.example.adapter.config;

public class ExampleHandler {

    private final String field;
    private final String message;

    public ExampleHandler(String field, String message) {
        this.field = field;
        this.message = message;
    }

    public String getField() {
        return field;
    }

    public String getMessage() {
        return message;
    }
}
