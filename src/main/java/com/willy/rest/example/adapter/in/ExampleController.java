package com.willy.rest.example.adapter.in;

import com.willy.rest.example.adapter.dto.request.ExampleRequest;
import com.willy.rest.example.adapter.dto.response.ExampleResponse;
import com.willy.rest.example.adapter.mapper.RequestMapper;
import com.willy.rest.example.adapter.mapper.ResponseMapper;
import com.willy.rest.example.adapter.openapi.ExampleApi;
import com.willy.rest.example.application.dto.output.ExampleResponseDTO;
import com.willy.rest.example.application.port.in.ExampleInputPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/example")
public class ExampleController implements ExampleApi {

    private final ExampleInputPort inputPort;
    private final RequestMapper requestMapper;
    private final ResponseMapper responseMapper;

    public ExampleController(ExampleInputPort inputPort, RequestMapper requestMapper, ResponseMapper responseMapper) {
        this.inputPort = inputPort;
        this.requestMapper = requestMapper;
        this.responseMapper = responseMapper;
    }

    @Override
    @PostMapping
    public ResponseEntity<Integer> create(@Valid @RequestBody ExampleRequest request) {
        return ResponseEntity.status(HttpStatus.CREATED).body(inputPort.create(requestMapper.toRequestDTO(request)));
    }

    @Override
    @GetMapping("/{id}")
    public ResponseEntity<ExampleResponse> getExample(@PathVariable int id) {
        Optional<ExampleResponseDTO> response = inputPort.getExample(id);
        return response.map(exampleResponseDTO -> ResponseEntity.ok(responseMapper.toResponse(exampleResponseDTO))).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
